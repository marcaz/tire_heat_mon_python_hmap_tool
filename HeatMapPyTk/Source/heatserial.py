import codecs
import os
import sys

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def ask_for_port():
    """\
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    index = 0
    n = 0
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:10} {!r}\n'.format(n, port, desc))
        print(desc)
        ports.append(port)
        if desc.find("Silicon") != -1:
            index = n - 1
            print("Port available:" + str(index))
        else:
            print("Port not available")
    if n == 0:
        return "NO_PORTS"
    else: 
        print("N is: " + str(n))
        ret_port = ports[index]
        print("Will ret this:" + ret_port)
        return ret_port

def open_port(port_id):
    
    print("Open port: " + port_id + " With baud: 38400")
    
    try:
        #serial_instance = serial.serial_for_url(
        #                port_id,
        #                baudrate=38400,
        #                parity='N',
        #                rtscts=False,
        #                xonxoff=False,
        #                do_not_open=True)
        #serial_instance.dtr = 1;  
        #serial_instance.open() 
        ser = serial.Serial(port_id,38400,timeout=250,parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, rtscts=0)
        ser.setDTR(1)
        print("Ar atidarytas(A): " + str(ser.is_open)) 
        return ser
        
    
    except serial.SerialException as e:
        sys.stderr.write('could not open port {!r}: {}\n'.format(port_id, e))
    else:
        print("Kaip ir okey")
    