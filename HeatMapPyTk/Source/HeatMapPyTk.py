import sys
import threading 
import time

import queue as Queue
import tkinter as tk

import heatserial
from test.test_string_literals import byte
from builtins import bytes

print_lock = threading.Lock()


temp_float_array = [0 for x in range(24*32)]


class myThread (threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print ("Starting " + self.name)
        print_time(self.threadID, self.name, 10, self.counter)
        print ("Exiting " + self.name)
      

def print_time(threadId, threadName, counter, delay):
    while counter:
        counter = 1
        parser_inst.poll_serial()

class data_parser(object):
    
    inst_active = 0
    
    def __init__(self):
        
        self.ba1 = bytearray()
        self.d_float_array = []
        
    def start_serial(self):
        
    
        act_port = heatserial.ask_for_port() 
        if(act_port.find("NO_PORTS")) != -1:
            sys.exit("Exiting due bad port")
        else:
            print("Port is OK")  
    
        self.ser_inst = heatserial.open_port(act_port)   
        return 1    
    
    def poll_serial(self):  
        
        ser_data = self.ser_inst.read() 
        #full_array.append(bytes(b'\x0B'))
        self.ba1 += ser_data
        if ser_data == b'\x0A':
            print("Last symbol") 
             
            if self.ba1.find(b'TEMPS:') != -1:
                ba2 = decode_floats_to_array(self.ba1)
                self.d_float_array = split_and_fill_float_array(ba2)
                print("PrefFloat: " + str(self.d_float_array))
            self.ba1.clear()
            
    def get_temper_float_array(self):
        if self.inst_active != 0:
            return self.d_float_array
        else: 
            sys.exit("parser inst not started")

      
def rgb(minimum, maximum, value):
    minimum, maximum = float(minimum), float(maximum)
    ratio = 2 * (value-minimum) / (maximum - minimum)

    b = int(max(0, 255*(1 - ratio)))
    r = int(max(0, 255*(ratio - 1)))
    g = 255 - b - r
    return r, g, b


def u2f_fetcher(u2f, queue):
    global tempvalues
    t = threading.currentThread()
    while getattr(t, "do_run", True):
        #u2f.updateFrame(tempvalues)
        queue.put(tempvalues)    


        
       


def decode_floats_to_array(serial_data):
    cmp_str = b'TEMPS:' 
    #print("Process len: " + str(cmp_str.__len__()))
    #print("Given data: " + str(serial_data))
    start_of_headr = serial_data.rfind(cmp_str)
    if start_of_headr != -1:
        n_ba = serial_data[start_of_headr + cmp_str.__len__():]
        #print("Processed: " + str(n_ba) + "Removing LF")
        n_ba = n_ba[:-2]
        print("Final: " + str(n_ba))
    else:
        sys.exit("No header")  
     
    return n_ba

def split_and_fill_float_array(read_ser_data):
    fl_array = []
    new_ba = read_ser_data.split(b',')
    for a in new_ba:
        fl_array.append(float(a))

    return fl_array
    
    
class TempView(tk.Tk):
    
    
    
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.pixelsize = 10
        self.cursorpos = 16 * 8 + 4

        self.title("USB2FIR - View_TK")

        self.tempmap = tk.Canvas(width=16*self.pixelsize, height=4*self.pixelsize)
        self.tempmap.pack()

        self.tempstr = tk.StringVar()
        self.label_temp = tk.Label(textvariable=self.tempstr)
        self.label_temp.pack()
        
        
     
        self.queue = Queue.Queue()

        self.updateMap()
        
        
        
    

    def updateMap(self):       
        #try: 24x32 Matrix
        tempvalues = [1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 100.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 19.7, 21.7, 25.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 18.7, 18.7, 15.7, 28.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 17.7, 35.7, 36.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 15.7, 16.7, 17.7, 25.7, 48.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7,
                      1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 6.7, 6.8, 7.1, 7.5, 8.5 , 8.6, 8.7, 12.7, 15.7, 15.7, 15.7, 15.7 , 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7]
        #tempvalues = self.queue.get_nowait()
        #print("process values")
        #nvar = 2
        
        my_fl_array = parser_inst.get_temper_float_array()
        
        print("We got this f array:" + str(my_fl_array))
       
        fl_cnt = 0
        for a in my_fl_array:
            fl_cnt += 1
    
        
        print("Floats count: " + str(fl_cnt))
    
        put_val_idx = 0
        var_cnt = 0
        for b in tempvalues:
            var_cnt += 1
            if (var_cnt > 256) and (put_val_idx < fl_cnt):  
                tempvalues[var_cnt] = my_fl_array[put_val_idx]
                put_val_idx += 1
                print("MOD")
        
        
        print("VAr count: " + str(var_cnt))
        print(tempvalues)

        self.setTempValues(tempvalues)
        #print("value sprocessed")
        #except Queue.Empty:
        #print ("Oops!Exec anyways")

        self.after(1000, self.updateMap)
        #print("Executing")


    def setTempValues(self, tempvalues):

        maximum = 0
        maximum_idx = 0
        minimum = 1000
        minimum_idx = 0
        i = 0
        for t in tempvalues:
            if t > maximum:
                maximum = t
                maximum_idx = i
            if t < minimum:
                minimum = t
                minimum_idx = i
            i = i + 1
        
        self.tempmap.delete("all")
        i = 0
        for t in tempvalues:
            row = i / 32
            column = 31 - i % 32
            x = column * self.pixelsize
            y = row * self.pixelsize
            color = '#%02x%02x%02x' % rgb(minimum, maximum, t)
            outline = color
            if i == self.cursorpos:
                outline = 'white'
            self.tempmap.create_rectangle(x, y, x + self.pixelsize - 1, y + self.pixelsize - 1, fill=color, outline=outline)

            i = i + 1
            
        self.tempstr.set('% 3.1f    % 3.1f    % 3.1f' % (minimum, tempvalues[self.cursorpos], maximum))
        


if __name__ == '__main__':
    
    print("BEGIN")
    
    parser_inst = data_parser()
    
    start_state = parser_inst.start_serial()
    
    parser_inst.inst_active = 1
    
    if start_state == 1:
        thread1 = myThread(1, "Thread-1", 1)
        thread1.daemon = True
        thread1.start()
    else:
        print("Failed to open port")

    
    app = TempView()
    app.mainloop()
    
    print("VERY_END") 


